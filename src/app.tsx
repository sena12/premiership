import * as React from 'react';
import {HeaderContainer} from './containers/headerContainer';
import {BodyContainer} from './containers/bodyContainer';
import './App.css'

export class App extends React.Component{
  render(){
    return (
      <div className="App" >
          <HeaderContainer />
          <br />
          <BodyContainer />
      </div>
    );
  }
}
