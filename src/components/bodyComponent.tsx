import * as React from 'react';
import '../App.css';

interface Props {
    round : string,
}

export class bodyComponent extends React.Component<Props>{
    data : any;
    selected : any;
    table : any;

    constructor(props){
        super(props);
        this.data = require('../data.json');
        this.table = [];
        this.createTable();
    }


    render(){
        if (this.props.round == '0'){
            return(
                <div className="Table_div">
                <div style={{marginLeft: '15%', marginRight: '15%', color: 'white', fontWeight: 800}}> 
                <h1 className="display-5">TABLE</h1>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">POS</th>
                            <th scope="col">CLUB NAME</th>
                            <th scope="col">GP</th>
                            <th scope="col">W</th>
                            <th scope="col">D</th>
                            <th scope="col">L</th>
                            <th scope="col">GF</th>
                            <th scope="col">GA</th>
                            <th scope="col">GD</th>
                            <th scope="col">LAST 5</th>
                            <th scope="col">PTS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.table.sort( (team1, team2) => { 
                                if (team1.pts > team2.pts) return -1;
                                else if (team1.pts < team2.pts) return 1;
                                else{
                                    if (team1.gd > team2.gd) return -1;
                                    else if (team1.gd < team2.gd) return 1;
                                    else {
                                        if (team1.gf > team2.gf) return -1;
                                        else if (team1.gf < team2.gf) return 1;
                                        else return 0;
                                    }
                                }
                            }).map( (team, index) => (
                                <tr>
                                    <td>{index+1}.</td>
                                    <td>{team.name}</td>
                                    <td>{team.gplayed}</td>
                                    <td>{team.gwon}</td>
                                    <td>{team.gdraw}</td>
                                    <td>{team.glost}</td>
                                    <td>{team.gf}</td>
                                    <td>{team.ga}</td>
                                    <td>{team.gd}</td>
                                    <td>{team.last5}</td>
                                    <td>{team.pts}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
                <div className="container" style={{textAlign: 'center'}}>
                        <p>
                        <b>GP - </b>games played<br />
                        <b>W - </b>won games<br />
                        <b>D - </b>draw games<br />
                        <b>L - </b>lost games<br />
                        <b>GF - </b>goals for<br />
                        <b>GA - </b>goals against<br />
                        <b>GD - </b>goals difference<br />
                        </p>
                </div>
                </div>
                </div>
        );
        }

        else {
            this.data.forEach( (element) => { if(element['round'] == this.props.round) this.selected = element['matches']; });
            return (
                <div className="Results_div">
                <div style={{marginLeft: '15%', marginRight: '15%', color: 'white', fontWeight: 800 }}> 
                <h1 className="display-5">ROUND {this.props.round}</h1>
                <table className="table table-hover" >
                    <tbody>
                      {
                          this.selected.map( match => (
                              <tr>
                                <td>{Object.keys(match)[0]}</td>
                                <td><b>{match[ Object.keys(match)[0] ]}</b></td>
                                <td><b>{match[ Object.keys(match)[1] ]}</b></td>
                                <td style={{textAlign: 'right'}} >{Object.keys(match)[1]}</td>
                              </tr>
                          ))
                      }  
                    </tbody>
                </table>
                </div>
                </div>
            );
        }
    }

    private createTable = () => {
        this.data[0]["matches"].forEach( (teams) => {
            this.table.push({name: Object.keys(teams)[0], gplayed: 0, gwon: 0, gdraw: 0, glost: 0, gf: 0, ga: 0, gd: 0, pts: 0, last5: ""}); 
            this.table.push({name: Object.keys(teams)[1], gplayed: 0, gwon: 0, gdraw: 0, glost: 0, gf: 0, ga: 0, gd: 0, pts: 0, last5: ""});
        })

        this.data.forEach((round) => {
            round["matches"].forEach( (match) => {
                if (match[Object.keys(match)[0]] > match[Object.keys(match)[1]]){
                    this.table.forEach ( (team) => { 
                        if( team.name == Object.keys(match)[0]){
                            team.pts = team.pts + 3;
                            team.gplayed = team.gplayed + 1;
                            team.gwon = team.gwon + 1;
                            team.gf = team.gf + match[Object.keys(match)[0]];
                            team.ga = team.ga + match[Object.keys(match)[1]];
                            team.gd = team.gf - team.ga;
                            if (team.gplayed > this.data.length - 5) team.last5 = team.last5 + " W";
                        }
                        if( team.name == Object.keys(match)[1]){
                            team.gplayed = team.gplayed + 1;
                            team.glost = team.glost + 1;
                            team.gf = team.gf + match[Object.keys(match)[1]];
                            team.ga = team.ga + match[Object.keys(match)[0]];
                            team.gd = team.gf - team.ga;
                            if (team.gplayed > this.data.length - 5) team.last5 = team.last5 + " L";
                        }
                    })

                }
                else if (match[Object.keys(match)[0]] < match[Object.keys(match)[1]]){
                    this.table.forEach ( (team) => { 
                        if( team.name == Object.keys(match)[1]){
                            team.pts = team.pts + 3;
                            team.gplayed = team.gplayed + 1;
                            team.gwon = team.gwon + 1;
                            team.gf = team.gf + match[Object.keys(match)[0]];
                            team.ga = team.ga + match[Object.keys(match)[1]];
                            team.gd = team.gf - team.ga;
                            if (team.gplayed > this.data.length - 5) team.last5 = team.last5 + " W";
                        }
                        if( team.name == Object.keys(match)[0]){
                            team.gplayed = team.gplayed + 1;
                            team.glost = team.glost + 1;
                            team.gf = team.gf + match[Object.keys(match)[1]];
                            team.ga = team.ga + match[Object.keys(match)[0]];
                            team.gd = team.gf - team.ga;
                            if (team.gplayed > this.data.length - 5) team.last5 = team.last5 + " L";
                        }
                    })
                }
                else{
                    this.table.forEach ( (team) => { 
                        if( team.name == Object.keys(match)[0]){
                            team.pts = team.pts + 1;
                            team.gplayed = team.gplayed + 1;
                            team.gdraw = team.gdraw + 1;
                            team.gf = team.gf + match[Object.keys(match)[0]];
                            team.ga = team.ga + match[Object.keys(match)[1]];
                            team.gd = team.gf - team.ga;
                            if (team.gplayed > this.data.length - 5) team.last5 = team.last5 + " D";
                        }
                        if( team.name == Object.keys(match)[1]){
                            team.pts = team.pts + 1;
                            team.gplayed = team.gplayed + 1;
                            team.gdraw = team.gdraw + 1;
                            team.gf = team.gf + match[Object.keys(match)[1]];
                            team.ga = team.ga + match[Object.keys(match)[0]];
                            team.gd = team.gf - team.ga;
                            if (team.gplayed > this.data.length - 5) team.last5 = team.last5 + " D";
                        }
                    })
                }
            })
        })
    }
}