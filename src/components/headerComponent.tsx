import * as React from 'react';


interface Props {
    round : string,
    switch : (id : string) => void
}

export class headerComponent extends React.Component<Props>{
    data : any;
    shown : boolean;

    constructor(props){
        super(props);
        this.shown = false;
    }

    componentWillMount(){
        this.data = require('../data.json');
        console.log(this.data);
    }

    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-light" onMouseLeave={() => { if(this.shown) {this.shown=false; this.forceUpdate();} } }>
                 <div className="collapse navbar-collapse" id="navbarColor02">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item dropdown show">
                            <a style={{cursor: 'pointer', color: 'black' }} className="nav-link dropdown-toggle" 
                            data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"
                            onMouseOver={() => { if(!this.shown) {this.shown=true; this.forceUpdate();} } }>ROUND</a>
                            
                        </li> 
                        <div className="dropdown-menu show" x-placement="bottom-start" 
                        onMouseLeave={() => {this.shown=false; this.forceUpdate();} }
                        style={{ display: `${this.shown ? 'inline' : 'none'}`, overflowY: 'auto', height: '500px', willChange: 'transform', position: 'absolute', top: '45px', left: '0px', transform: 'translate3d(0px, 37px, 0px)'}} > 
                        {
                            this.data.map(round => ( 
                            <div>
                                <a onClick={(e : any) => { this.shown = false; this.props.switch(round['round']); } } 
                                className="dropdown-item"
                                style={{cursor: 'pointer'}}> 
                                {round['round']}</a>
                                <br />
                            </div>))
                        }
                       </div>
                     <li className="nav-item active">
                        <a className="nav-link" onClick={(e : any) => this.props.switch('0')  } style={{cursor: 'pointer', color: 'black'}}>TABLE<span className="sr-only">(current)</span></a>
                        </li>
                    
                        <br />   
                        
                </ul>
                </div>
                <img src="http://a1.espncdn.com/combiner/i?img=%2Fi%2Fleaguelogos%2Fsoccer%2F500%2F23.png" height="75px" width="75px" style={{float: 'right'}}/>

            </nav>
        );
    }
}