import { connect } from 'react-redux';
import { State } from '../reducers/index';
import { showRound } from '../actions/actionTypes';
import { headerComponent } from '../components/headerComponent';

const mapStateToProps = (state : State) => {
    return { round : state.headerReducer.round }
}

const dispatchStateToProps = (dispatch) => {
    return { switch : (id : string) => dispatch(showRound(id)) }
}

export const HeaderContainer = connect(
    mapStateToProps,
    dispatchStateToProps
)(headerComponent);