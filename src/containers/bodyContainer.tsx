import { connect } from 'react-redux';
import { State } from '../reducers/index';
import { showRound } from '../actions/actionTypes';
import { bodyComponent } from '../components/bodyComponent';

const mapStateToProps = (state : State) => {
    return { round : state.bodyReducer.round }
}

const dispatchStateToProps = (dispatch) => {
    return {  }
}

export const BodyContainer = connect(
    mapStateToProps,
    dispatchStateToProps
)(bodyComponent);