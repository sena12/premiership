import {actionEnums} from '../types/enums';

export interface headerState {
    round : string
}

export const defaultHeaderState: () => headerState = () => ({
    round : length()
});

export const headerReducer = (state = defaultHeaderState(), action) => {
    switch(action.type){
        case actionEnums.SHOW_ROUND: { return ({...state, round: action.round}); }
    }
    return state;
}

function length(){
    let data = require('../data.json');
    return data.length.toString();
}