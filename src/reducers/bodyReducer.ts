import {actionEnums} from '../types/enums';

export interface bodyState {
    round : string
}

export const defaultBodyState: () => bodyState = () => ({
    round : length()
});

export const bodyReducer = (state = defaultBodyState(), action) => {
    switch(action.type){
        case actionEnums.SHOW_ROUND: { return ({...state, round: action.round}); }
    }
    return state;
}
 
function length(){
    let data = require('../data.json');
    return data.length.toString();
}