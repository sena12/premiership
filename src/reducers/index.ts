import { combineReducers } from 'redux';
import { headerReducer, headerState } from './headerReducer';
import { bodyReducer, bodyState } from './bodyReducer'; 

export interface State {
    headerReducer : headerState
    bodyReducer : bodyState
}

export const reducers = combineReducers({
    headerReducer,
    bodyReducer
})
