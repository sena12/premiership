import {actionEnums} from '../types/enums';

export const showRound = (id: string) => ({
    type: actionEnums.SHOW_ROUND,
    round : id
});